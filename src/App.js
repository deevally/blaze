//import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
   
      <h1>
      Privacy Policy And Terms of Use
      </h1>
      {/* <header className="App-header">
        
        <p>

        </p>
        
      </header> */}
      <h3>
      WHAT INFORMATION DO WE COLLECT?
      </h3>

    <p><b>None.</b> We do not collect any information of our users before, during, or even after using our VPN  service. We believe in 100% privacy for all our users.</p>

    <h3>DO WE USE COOKIES?</h3>
    <p>No.</p>

<h3>DO WE DISCLOSE ANY INFORMATION TO OUTSIDE PARTIES?</h3>

<p><b>No.</b> Unlike other VPN providers, We DO NOT collect any information of our users. We are committed to protecting your privacy.</p>
{/* <h3>IS THIS APP SAFE FOR CHILDREN UNDER THE AGE OF 13?</h3>
<p>Yes it is. Our app does not contain any materials unsuitable for children.</p> */}

<h3>THIRD PARTY ADVERTISING</h3>

<p>For users who wish to use our product offering for free, we offer an advertising supported version of BlazeSurf VPN. In this version, users watch ads from our various ad partners, earnings from which are put towards keeping multiple servers running thus ensuring we offer the best possible service to our users. We absolutely DO NOT collect or store any user data on our end. Our aim is to offer a good user experience, maintain your privacy and honor your trust in us. If you wish to learn more about the privacy policy of our ad partners click on their respective links –  <a href="https://policies.google.com/privacy?hl=en" target="_blank" rel="noreferrer">AdMob</a> <a href="https://developers.facebook.com/docs/audience-network/policy/" target="_blank" rel="noreferrer">Facebook Ad</a></p>
<h3>YOUR CONSENT
</h3>
<p>By using Blazesurf VPN, you consent to our privacy policy.
</p>

<h3>CHANGES TO OUR PRIVACY POLICY
</h3>
<p>If we decide to change our privacy policy, we will post those changes on this page.</p>
<h3>CONTACTING US
</h3>

<p>If there are any questions regarding this privacy policy you may contact us via email using <b>quotechc@gmail.com</b>
</p>
    </div>
  );
}

export default App;
